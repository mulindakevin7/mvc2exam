<?php
namespace App\Model;
use Core\App;
use Core\Kernel\AbstractModel;

class DefaultModel extends AbstractModel{
    protected static $table ='user';
    protected $id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }
    protected $nom;
    protected $email;

    //insert

    public static function insert($post)
    {
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (nom, email) VALUES (?,?)",
            array($post['nom'], $post['email'])
        );
    }

}