<?php

namespace App\Controller;

use App\Model\DefaultModel;
use App\Model\SalleModel;
use App\Service\Form;
use Core\Kernel\AbstractController;
use App\Service\Validation;

/**
 *
 */
class DefaultController extends AbstractController
{
    public function index()
    {
        $message = 'Bienvenue sur le framework MVC';
        $this->render('app.default.frontpage',array(
            'users'=>DefaultModel::all(),
            'message' => $message,
        ));
    }

    /**
     * Ne pas enlever
     */
    public function Page404()
    {
        $this->render('app.default.404');
    }

    public function add(){
        $errors =array();
        if (!empty($_POST['submitted'])){
            //faille xss
            $post = $this->cleanXss($_POST);
            //valida..
            $v = new Validation();
            $errors = $this->validate($v,$post);
            //if errors
            if ($v->IsValid($errors)){
                DefaultModel::insert($post);
                //flash mss..
                $this->addFlash('success','nouvelle user ajouté');
                //redi..
                $this->redirect('user_list');
            }
        }
        $form = new Form($errors);
        $this->render('app.default.add',array(
            'form'=>$form,
            'users'=>DefaultModel::all()
        ));
    }
    private function validate($v,$post)
    {
        $errors = [];
        $errors['nom'] = $v->textValid($post['nom'], 'nom',3, 150);
        $errors['email'] = $v->textValid($post['email'], 'email',3, 150);
        return $errors;
    }
    public function addSalle(){

    }
    public function listingSalle(){
        $message = 'list de salles';
        $this->render('app.default.frontpage',array(
            'message'=>$message,
            'salles'=>SalleModel::all()

        ));
    }
}
