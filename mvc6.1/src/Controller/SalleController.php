<?php

namespace App\Controller;
use App\Model\SalleModel;
use App\Service\Form;
use Core\Kernel\AbstractController;
use App\Service\Validation;

class SalleController extends AbstractController{
    public function listingSalle(){
        $this->render('app.admin.index',array(
            'salles' => SalleModel::all(),
        ),'admin');


    }

    public function addSalle(){
        $errors = array();
        if (!empty($_POST['submitted'])){
            //fail xss
            $post = $this->cleanXss($_POST);
            //validation
            $v = new Validation();
            $errors = $this->validate($v,$post);
            if ($v->IsValid($errors)){
                SalleModel::insert($post);
                //flash mssg
                $this->addFlash('success','nouvelle salle ajouté');
                //redirection
                $this->redirect('salles');
            }

        }
        $form = new Form($errors);
        $this->render('app.admin.add', array(
            'form' => $form,
            'contacts' => SalleModel::all()
        ));


    }
    private function validate($v,$post)
    {
        $errors = [];
        $errors['title'] = $v->textValid($post['title'], 'title',3, 150);
        $errors['maxuser'] = $v->textValid($post['maxuser'], 'maxuser',0, 150);
        return $errors;
    }
}